package br.com.senac.cachoeira.dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObservable;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.model.Cachoeira;

public class CachoeiraDAO extends SQLiteOpenHelper {

    private static final String DATABASE = "SQLiteCachoeira";
    private static int VERSION = 1;


    public CachoeiraDAO(Context context) {
        super(context, DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String ddl = "CREATE TABLE Cachoeiras (id INTEGER PRIMARY KEY, nome TEXT NOT NULL, info TEXT, imagem TEXT, classificacao REAL); ";
        sqLiteDatabase.execSQL(ddl);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String ddl = "DROP TABLE IF EXISTS Cachoeiras;";
        sqLiteDatabase.execSQL(ddl);
        this.onCreate(sqLiteDatabase);
    }


    public void salvar (Cachoeira cachoeira){
        ContentValues values = new ContentValues();
        values.put("nome", cachoeira.getNome());
        values.put("info", cachoeira.getInformocoes());
        values.put("imagem", cachoeira.getImagem());
        values.put("classificacao", cachoeira.getClassificacao());

        if(cachoeira.getId() == 0) {
            getWritableDatabase().insert("Cachoeiras", null, values);
        }else{
            getWritableDatabase().update("Cachoeiras", values, " id = " + cachoeira.getId() ,null );
        }


    }



    public List<Cachoeira> getLista(){
        List<Cachoeira> lista = new ArrayList<>();
        String colunas[] = {"id", "nome", "info", "classificacao","imagem"};

        Cursor cursor = getWritableDatabase().query("Cachoeiras",colunas,null,null,null,null,null);

        while(cursor.moveToNext()){

            Cachoeira cachoeira = new Cachoeira();

            cachoeira.setId(cursor.getInt(0));
            cachoeira.setNome(cursor.getString(1));
            cachoeira.setInformocoes(cursor.getString(2));
            cachoeira.setClassificacao ((Double)(cursor.getDouble(3)));
            cachoeira.setImagem(cursor.getString(4));

            lista.add(cachoeira);
        }

        return lista;
    }


}
