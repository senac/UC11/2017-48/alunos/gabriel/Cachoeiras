package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.model.Cachoeira;

public class DetalheActivity extends AppCompatActivity {

    private Cachoeira cachoeira;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        Intent intent = getIntent();


        cachoeira = (Cachoeira) intent.getSerializableExtra(MainActivity.CACHOEIRA);

        if (cachoeira != null) {


            TextView textViewNome = findViewById(R.id.titulo);
            TextView textViewInformacoes = findViewById(R.id.informacoes);
            RatingBar ratingBarClassificacao = findViewById(R.id.rtClassificacao);
            ImageView imageView = findViewById(R.id.foto);


            if (cachoeira.getImagem() != null) {
                Bitmap fotoOriginal = BitmapFactory.decodeFile(cachoeira.getImagem());

                Bitmap fotoReduzida = Bitmap.createScaledBitmap(fotoOriginal, 200, 200, true);

                imageView.setImageBitmap(fotoReduzida);
            } 

            textViewNome.setText(cachoeira.getNome());
            textViewInformacoes.setText(cachoeira.getInformocoes());
            ratingBarClassificacao.setRating((float) cachoeira.getClassificacao());

        }


    }
}
