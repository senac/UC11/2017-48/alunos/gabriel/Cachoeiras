package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import java.io.File;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.dao.CachoeiraDAO;
import br.com.senac.cachoeira.model.Cachoeira;

public class NovoActivity extends AppCompatActivity {

    private static final int CAPTURA_IMAGEM = 1  ;


    private ImageView imageView;
    private EditText editTextNome;
    private EditText editTextInformacoes;
    private RatingBar ratingBarClassificacao;
    private Button buttonSalvar;

    private Cachoeira cachoeira;

    private CachoeiraDAO dao;

    private String nomeArquivo ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);
        imageView = findViewById(R.id.foto);
        editTextNome = findViewById(R.id.nome);
        editTextInformacoes = findViewById(R.id.informacoes);
        ratingBarClassificacao = findViewById(R.id.rtClassificacao);
        buttonSalvar = findViewById(R.id.btnSalvar);

        Intent intent = getIntent();

        cachoeira = (Cachoeira) intent.getSerializableExtra(MainActivity.CACHOEIRA);


        if (cachoeira == null) {
            cachoeira = new Cachoeira();
        } else {
            preencherActivity(cachoeira);
        }
    }




    public void salvar(View view) {

        String nome = editTextNome.getText().toString();
        String informacao = editTextInformacoes.getText().toString();
        float classificacao = ratingBarClassificacao.getRating();
        String imagem = nomeArquivo;

        cachoeira.setNome(nome);
        cachoeira.setInformocoes(informacao);
        cachoeira.setClassificacao(classificacao);
        cachoeira.setImagem(imagem);

        /* Adiconando ao Banco*/

        try {
            dao = new CachoeiraDAO(NovoActivity.this);
            dao.salvar(cachoeira);
            dao.close();

            Toast.makeText(NovoActivity.this, "Salvo com Sucesso", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(NovoActivity.this, "Erro ao Salvar", Toast.LENGTH_LONG).show();
        }


        finish();

    }

    private void preencherActivity(Cachoeira cachoeira) {

        editTextNome.setText(cachoeira.getNome());
        editTextInformacoes.setText(cachoeira.getInformocoes());
        ratingBarClassificacao.setRating((float) cachoeira.getClassificacao());

        if(cachoeira.getImagem() != null) {
            Bitmap fotoOriginal = BitmapFactory.decodeFile(cachoeira.getImagem());

            Bitmap fotoReduzida = Bitmap.createScaledBitmap(fotoOriginal, 200, 200, true);

            imageView.setImageBitmap(fotoReduzida);
        }

    }

    public void pegarImagem(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String caminhoDoArquivo = Environment.getExternalStorageDirectory().toString() ;


        nomeArquivo = caminhoDoArquivo + "/" + System.currentTimeMillis() + ".png";

        File arquivoImagem = new File(nomeArquivo) ;

        Uri localDeArmazenamento = Uri.fromFile( arquivoImagem ) ;

        intent.putExtra(MediaStore.EXTRA_OUTPUT , localDeArmazenamento ) ;



        startActivityForResult(intent , CAPTURA_IMAGEM);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(CAPTURA_IMAGEM == requestCode){
            if(resultCode == RESULT_OK){


                Bitmap fotoOri = BitmapFactory.decodeFile(nomeArquivo) ;

                Bitmap fotoFormt = Bitmap.createScaledBitmap(fotoOri , 200 , 200 , true);

                imageView.setImageBitmap(fotoFormt);

            }
        }

    }
}
